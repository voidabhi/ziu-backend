<?php

class Booking extends Illuminate\Database\Eloquent\Model {
    public $timestamps = false;
    protected $table = 'bookings';
    protected $fillable = ['uname', 'uphone', 'fid'];
}
