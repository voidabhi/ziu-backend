<?php

class Feed extends Illuminate\Database\Eloquent\Model {
    public $timestamps = false;
    protected $table = 'feeds';
    protected $hidden = ['sid'];
    protected $fillable = ['tag', 'imgurl'];

    public function salon() {
        return $this->belongsTo('Salon', 'sid', 'id');
    }
}
