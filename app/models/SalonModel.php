<?php

class Salon extends Illuminate\Database\Eloquent\Model {
    public $timestamps = false;
    protected $table = 'salons';
    protected $fillable = ['name', 'phone'];

    public function feeds() {
        return $this->hasMany('Feed', 'sid', 'id');
    }
}
