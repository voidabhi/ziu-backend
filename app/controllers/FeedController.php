<?php

use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;
use \Slim\Log;

class FeedController extends \SlimController\SlimController
{
    public function index() {

        $id = isset($_GET['id'])  ? $_GET['id'] : '';
        $count = isset($_GET['count'])  ? $_GET['count'] : 6;

        $feed = Feed::where('id','=',$id)->first();

        if($feed) {
            $feed['similarfeeds'] = Feed::where('sid', '=', $feed->salon->id)->take($count)->get();
        }

        return $this->app->response->setBody($feed ? $feed : '{}');
    }

    public function all()
    {

        $sid = isset($_GET['sid'])  ? $_GET['sid'] : false;
        $count = isset($_GET['count']) ? $_GET['count'] : 30;

        if (!$sid) {
            $feeds = Feed::all()->take($count);
        } else {
            $feeds = Feed::where('sid', '=', $sid)->take($count)->get();
        }


        $feeds->each(function($feed){
            $feed['salon'] = $feed->salon;
        });
        return $this->app->response->setBody($feeds);
    }
}
