<?php



class BookingController extends \SlimController\SlimController
{
    public function add()
    {
        $uname = $this->app->request()->post('uname');
        $uphone = $this->app->request()->post('uphone');
        $feedid = $this->app->request()->post('fid');

        if(!isset($uname, $uphone, $feedid)) {
            return $this->app->response->setBody(json_encode(array(
                'error' => 'Incomplete Form'
            )));
        } else {
            Booking::create([
                'uname' => $uname,
                'uphone' => $uphone,
                'fid' => $feedid
            ]);

            $feed = Feed::where('id', '=', $feedid)->first();

            MailService::sendMail($uname, $uphone, $feed);

            $this->app->response->setBody(json_encode([
                    'success' => true
                ]));
        }
    }
}
