<?php

/*
* Feed Routes
*/

return array(
    '/feeds' => 'Feed:all',
    '/feed' => 'Feed:index'
);
