<?php


class MailService {

    public static function sendMail($uname, $uphone, $feed) {
        $mail = new PHPMailer;

        //Enable SMTP debugging.
        $mail->SMTPDebug = 0;
        //Set PHPMailer to use SMTP.
        $mail->isSMTP();
        //Set SMTP host name
        $mail->Host = "smtp.gmail.com";
        //Set this to true if SMTP host requires authentication to send email
        $mail->SMTPAuth = true;
        //Provide username and password
        $mail->Username = "contact.ziuapp@gmail.com"; // EMAIL ID HERE
        $mail->Password = "z26i9u21"; // PASSWORD HERE
        //If SMTP requires TLS encryption then set it
        $mail->SMTPSecure = "tls";
        //Set TCP port to connect to
        $mail->Port = 587;

        $mail->From = "contact.ziuapp@gmail.com";
        $mail->FromName = "ZIU";

        $message = 'Ziu user <b>'.$uname.'</b> has made booking for <b>#'.$feed->tag.'</b> from salon <b>' . $feed->salon->name . '</b>.<br> User Contact Info: <b>' . $uphone . '</b>. Salon Contact Info: <b>' . $feed->salon->phone . '</b>';
        $emails = explode(",", 'abhijeetshibu@gmail.com, abhijeetshibu@gmail.com');

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        for ($i=0; $i < sizeof($emails); $i++) {
            $mail->addAddress($emails[$i], "Ziu Team Member");
        }

        $mail->isHTML(true);

        $mail->Subject = "ZIU HAS GOT NEW BOOKING";
        $mail->Body = $message;
        $mail->AltBody = $message;

        $mail->send();
    }
}
