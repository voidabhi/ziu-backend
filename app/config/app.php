<?php

/*
 *  Initializing Slim application
 */

\Slim\Slim::registerAutoloader();

$app = new \SlimController\Slim(array(
                            'cookies.secret_key'         => md5('appsecretkey'),
                            'controller.class_prefix'    => '',
							'controller.class_suffix'    => 'Controller',
                            'controller.method_suffix'   => ''
                     ));

$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);

$app->add($cors);

/**
 * Require all models
 */
foreach (glob(APP_PATH."/models/*.php") as $filename) {
    require $filename;
}

/**
 * Require all controllers
 */
foreach (glob(APP_PATH."/controllers/*.php") as $filename) {
    require $filename;
}

/**
 * Require all routes
 */
foreach (glob(APP_PATH."/routes/*.php") as $routes) {
	$app->addRoutes(require $routes);
}

require APP_PATH."/hooks.php";
require APP_PATH."/helpers/OAuthMiddleware.php";
require APP_PATH."/helpers/MailService.php";

$headers = apache_request_headers();
$app->add(new OAuth2Auth($headers));

$app->error(function (\Exception $e) use ($app) {
    $app->render(array(
            'error' => $e->getMessage()
        ));
});

$app->run();
